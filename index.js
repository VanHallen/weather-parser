const axios = require('axios');
const htmlparser2 = require("htmlparser2");

let inCity = false;
let inProvince = false;
let inDate = false;
let inTempTable = false;
let inTemp = false;
let done = false;

let city = '';
let date = 0;

const parser = new htmlparser2.Parser(
    {
        onattribute(name, value) {
            if ( name === "id" && value === "wb-cont") {
                inCity = true;
            }
            if ( name === "class" && value === "mrgn-bttm-0") {
                date += 1;
                if (date == 2) {
                    inDate = true;
                }
            }
            if (name === "class" && value === "dl-horizontal wxo-conds-col2") {
                inTempTable = true;
            }
            if (name === "class" && value === "mrgn-bttm-0 wxo-metric-hide") {
                inTemp = true;
            }
        },
        onopentag(name, attribs) {
            
            if ( name === "abbr" && inCity) {
                inProvince = true;
                inCity = false;
            }
        },
        ontext(text) {
            if (inCity) {
                city += ( "City: " + text );
            }
            if(inProvince) {
                city += text;
                inProvince = false;
            }
            if (inDate) {
                console.log("Date: " + text);
                inDate = false;
            }
            if (inTemp && !done) {
                console.log("Temperature: " + text);
                inTemp = false;
                inTempTable = false;
                done = true;
            }
        },
        onclosetag(tagname) {
            if(tagname === "h1") {
                console.log(city);
            }
        },
     
    },
    { decodeEntities: true }
);

if ( process.argv.length > 2 ) {
    axios.get( process.argv[2] )
        .then( response => {
            parser.write( response.data );
            parser.end();
        })
        .catch( error => {
            console.log("Could not fetch page.");
        });
} else {
    console.log( "Missing URL argument" );
}